#include "../TrigCountDumper.h"
#include "../TrigDecisionChecker.h"
#include "../TrigEDMChecker.h"
#include "../TrigEDMAuxChecker.h"
#include "../TrigSlimValAlg.h"

DECLARE_COMPONENT( TrigCountDumper )
DECLARE_COMPONENT( TrigDecisionChecker )
DECLARE_COMPONENT( TrigEDMChecker )
DECLARE_COMPONENT( TrigEDMAuxChecker )
DECLARE_COMPONENT( TrigSlimValAlg )
