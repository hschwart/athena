#include "../TrigInDetTrackFitter.h"
#include "../TrigInDetBremDetectionTool.h"
#include "../TrigDkfTrackMakerTool.h"
#include "../TrigL2ResidualCalculator.h"

DECLARE_COMPONENT( TrigInDetTrackFitter )
DECLARE_COMPONENT( TrigInDetBremDetectionTool )
DECLARE_COMPONENT( TrigDkfTrackMakerTool )
DECLARE_COMPONENT( TrigL2ResidualCalculator )
